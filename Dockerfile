FROM tarantool/tarantool:1.10.2
COPY /src/* /opt/tarantool
CMD ["tarantool", "/opt/tarantool/app.lua"]